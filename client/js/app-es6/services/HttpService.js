export class HttpService {

    constructor() {
        // this._xhr = new XMLHttpRequest();
    }

    _handleErrors(res) {
        if(!res.ok) throw new Error(res.statusText);
            return res;
    }

    get(url) {

        return fetch(url)
            .then(res => this._handleErrors(res))
            .then(res => res.json());

        /* return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.onreadystatechange = () => {
                //  0: requisicao não iniciada
                //  1: conexao estabelecida
                //  2: requisição recebida
                //  3: processando requisição
                //  4: requisição concluida e resposta pronta
                if(xhr.readyState == 4){

                    if (xhr.status == 200) {

                        
                        resolve(JSON.parse(xhr.responseText));
                                                        
                    } else {
                        console.log(xhr.responseText);
                        console.log('Houve erro na requisição');
                        reject(xhr.responseText)  
                    }
                } 
            };
            xhr.send();
        }); */
    }

    post(url, dado) {

        return fetch(url, {
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            body: JSON.stringify(dado)
        })
        .then(res => this._handleErrors(res));

        /* return new Promise((resolve, reject) => {
    
            let xhr = new XMLHttpRequest();
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.onreadystatechange = () => {
    
                if (xhr.readyState == 4) {
    
                    if (xhr.status == 200) {
    
                        resolve(JSON.parse(xhr.responseText));
                    } else {
    
                        reject(xhr.responseText);
                    }
                }
            };
            xhr.send(JSON.stringfy(dado));
        }); */
    }
}