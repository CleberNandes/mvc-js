import { HttpService } from './HttpService';
import { ConnectionFactory } from '../database/ConnectionFactory';
import { NegociacaoDao } from '../dao/NegociacaoDao';
import { Negociacao } from '../models/Negociacao';

export class NegociacaoService {

    constructor() {
        this._http = new HttpService();
    }

    create(negociacao) {
        return ConnectionFactory.getConnection()
        .then(conexao => new NegociacaoDao(conexao))
        .then(dao => dao.create(negociacao))
        .then(() => 'Negociação cadastrada com sucesso')
        .catch(erro => {
            console.log(erro);
            
            throw new Error("Não foi possível adicionar a negociação")
        });
    }

    index() {
        return ConnectionFactory.getConnection()
        .then(conexao => new NegociacaoDao(conexao))
        .then(dao => dao.index())
        .catch(erro => {
            console.log(erro);
            
            throw new Error("Não foi possível adicionar a negociação")
        });
    }

    delete(negociacao) {
        return ConnectionFactory.getConnection()
        .then(conexao => new NegociacaoDao(conexao))
        .then(dao => dao.delete(negociacao))
        .catch(erro => {
            console.log(erro);
            throw new Error("Não foi possível deletar as negociações")
        });
    }

    importa(listaAtual) {

        return this._todas()
            .then(negociacoes =>
                negociacoes.filter(negociacao =>
                    !listaAtual.some(negociacaoExistente =>
                        JSON.stringify(negociacao) == JSON.stringify(negociacaoExistente)))
            )
            .catch(erro => {
                console.log(erro);
                throw new Error("Não foi possível importar as negociações");
            });
    }

    _todas() {
        return new Promise((resolve,reject) => {
            Promise.all([
                this.obtemDaSemana(),
                this.obtemAnterior(),
                this.obtemRetrasada()
            ])
            .then(resp => {
                resolve(
                    resp.reduce((novoArray, array) => novoArray.concat(array), [])
                );       
            })
            .catch(erro => reject(erro));
        })
    }

    obtemDaSemana() {
        return new Promise((resolve, reject) =>
            this._http.get('negociacoes/semana')
                .then(resp => resolve(this._returnResp(resp)))
                .catch(erro => {
                    reject('Erro ao buscar as negociações da semana');
                    console.log(erro);
                }));
    }

    obtemAnterior() {
        return new Promise((resolve, reject) =>
            this._http
                .get('negociacoes/anterior')
                .then(resp => resolve(this._returnResp(resp)))
                .catch(erro => {
                    reject('Erro ao buscar as negociações da semana anterior');
                    console.log(erro);
                }));
    }

    obtemRetrasada() {
        return new Promise((resolve, reject) =>
            this._http.get('negociacoes/retrasada')
            .then(resp => resolve(this._returnResp(resp)))
            .catch(erro => {
                reject('Erro ao buscar as negociações da semana retrasada');
                console.log(erro);
            }));
    }

    _returnResp(resp) {
        return resp.map(item => new Negociacao(
            new Date(item.data),
            item.quantidade,
            item.valor
        ));
    }
}