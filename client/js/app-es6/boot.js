import { currentInstance } from './controllers/NegociacaoController';
import {} from './polyfill/fetch';

let negociacaoController = currentInstance();

document.querySelector('.form').onsubmit = negociacaoController.adiciona.bind(negociacaoController);
document.querySelector('.delete').onclick = negociacaoController.delete.bind(negociacaoController);
document.querySelector('.importa').onclick = negociacaoController.importa.bind(negociacaoController);



