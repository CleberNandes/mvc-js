import { View } from './View';
import { Mensagem } from '../models/Mensagem';
export class MensagemView extends View {

    constructor(elemento) {
        super(elemento);
    }

    update(model) {
      super.update(model);
      setTimeout(() => {
        super.update(new Mensagem(''));
      }, 3000);
    }
  
    _template(model) {

      return model.texto ? `
        <p class="alert alert-info">
          ${model.texto}
        </p>` : '';
    }
    
  }  