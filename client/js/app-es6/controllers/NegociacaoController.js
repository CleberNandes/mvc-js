import { Bind } from '../helpers/Bind';
import { Negociacoes } from '../models/Negociacoes';
import { Negociacao } from '../models/Negociacao';
import { Mensagem } from '../models/Mensagem';
import { NegociacoesView } from '../views/NegociacoesView';
import { MensagemView } from '../views/MensagemView';
import { NegociacaoService } from '../services/NegociacaoService';
import { DateHelper } from '../helpers/DateHelper';

class NegociacaoController {

    constructor() {
        let $ = document.querySelector.bind(document);
        this._data = $('#data');
        this._quantidade = $('#quantidade');
        this._valor = $('#valor');

        this._property = null;

        this._negociacoes = new Bind(
            new Negociacoes(),
            new NegociacoesView($('#negociacoes-view')),
            'create', 'delete', 'lista'
        );
        this._mensagem = new Bind(
            new Mensagem(),
            new MensagemView($('#mensagem-view')),
            'texto'
        );
        this._service = new NegociacaoService();
        this._init();
    }

    _init() {
        this._service.index()
        .then(negociacoes => negociacoes.forEach(
            item => this._negociacoes.create(item)))
        .catch(error => console.log(error));

        setInterval(() => {
            this.importa();
        }, 3000);
    }

    ordena(property) {
        
        this._negociacoes.lista = this._negociacoes.lista.sort( (a, b) => (
            a[`${property}`] > b[`${property}`] && property != this._property
        ) ? 1 : -1);
        this._property = property;
    }

    adiciona(event) {
        event && event.preventDefault();
        let negociacao = this._criaNegociacao()
        this._service.create(negociacao)
        .then(msg => {
            this._negociacoes.create(negociacao);
            this._mensagem.texto = msg;
            this._limpaFormulario();
        }).catch(erro => this._mensagem.texto = erro);
    }

    delete() {
        this._service.delete()
            .then((msg) => {
                this._negociacoes.delete();
                this._mensagem.texto = msg;
            }).catch(error => console.log(error))
    }

    importa() {
        this._service.importa(this._negociacoes.lista)
            .then(negociacoes => {                
                negociacoes.forEach(nego => this._negociacoes.create(nego));
                if(negociacoes.length) {
                    this._mensagem.texto = 'Negociações importadas!'    
                }
            })
            .catch(erro => this._mensagem.texto = erro); 
    }

    _criaNegociacao() {
        return new Negociacao(
            DateHelper.textoToDate(this._data.value),
            parseInt(this._quantidade.value, 10),
            parseFloat(this._valor.value)
        );
    }

    _limpaFormulario() {
        this._data.value = '';
        this._quantidade.value = 1;
        this._valor.value = 0.0
        this._data.focus();
    }
}

let negociacaoController = new NegociacaoController();

export function currentInstance() {

    return negociacaoController;

}