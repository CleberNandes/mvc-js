export class DateHelper {

    constructor() {

        throw new Error('DateHelper é uma classe estática e não pode ser instanciada');
    }

    static textoToDate(texto) {
        if(!/\d{4}-\d{2}-\d{2}/.test(texto))
            throw new Error('Deve estar no formato aaaa-mm-dd');
        return new Date(...
            texto.split('-')
                .map((item, index) => item - index % 2)
        );
    }

    static dateToTexto(data) {
        return `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;
    }
}