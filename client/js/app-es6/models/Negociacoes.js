export class Negociacoes {

    constructor(model) {
        this._negociacoes = [];
    }

    create(negociacao) {
        this._negociacoes.push(negociacao);
    }

    get lista() {
        return [].concat(this._negociacoes);
    }

    set lista(lista) {
        this._negociacoes = lista;
    }

    delete() {
        this._negociacoes = [];
    }
}