import { Negociacao } from '../models/Negociacao';

export class NegociacaoDao {

    constructor(connection = null) {
        this._store = 'negociacoes';
        this._connection = connection;
    }

    create(negociacao) {
        return new Promise((resolve, reject) => {
            let request = this._connection
                .transaction([this._store], 'readwrite')
                .objectStore(this._store)
                .add(negociacao);
            request.onsuccess = () => resolve();
            request.onerror = (error) => reject(error);
        });
    }

    index() {
        return new Promise((resolve, reject) => {

            let cursor = this._connection
                .transaction([this._store], 'readwrite')
                .objectStore(this._store)
                .openCursor();
            let negociacoes = [];
            cursor.onsuccess = (e) => {
                let item = e.target.result;
                if(item) {
                    negociacoes.push(new Negociacao(
                        item.value._data,
                        item.value._quantidade,
                        item.value._valor
                    ));
                    item.continue();
                } else {
                    resolve(negociacoes)
                }
            };
            cursor.onerror = e => {
                console.log(e.target.error);
                reject('Não foi possível listar as negociações');
            }
        });
    }

    delete() {
        return new Promise((resolve, reject) => {
            let request = this._connection
                .transaction(this._store, 'readwrite')
                .objectStore(this._store)
                .clear();
            request.onsuccess = () => resolve('Negociações removidas!');
            request.onerror = e => {
                console.log(e.target.error);
                reject('Erro ao remover as negociações!')
            };
        })
    }
}