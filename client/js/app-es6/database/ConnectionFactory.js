
const dbName = 'cleberframe';
const models = ['negociacoes'];
const versao = 4;
let connection = null;
let close = null;

export class ConnectionFactory {
    constructor(){
        throw new Error('Factory não pode ser instanciada!')
    }

    static getConnection() {
        return new Promise((resolve, reject) => {
            let openRequest = window.indexedDB.open(dbName, versao);

            openRequest.onupgradeneeded = (e) => {
                ConnectionFactory._createModels(e.target.result); 
            }

            openRequest.onsuccess = (e) => {
                if(!connection)
                    connection = e.target.result;
                close = connection.close.bind(connection);
                connection.close = () => {
                    throw new Error('Somente a classe pode fechar a conexão');
                }
                resolve(connection);
            }

            openRequest.onerror = (e) => {
                reject('Não foi possível obter uma conexão');
                console.log(e.target.error);
            }
        });
    }

    static _createModels(connection) {
        models.forEach(item => {
            if(connection.objectStoreNames.contains(item))
                connection.deleteObjectStore(item);
            connection.createObjectStore(item, {autoIncrement: true});
        })
    }

    static _closeConnection() {
        if(connection) {
            close();
            connection = null;
        } else
            console.log('Não existe conexão aberta');
    }
}